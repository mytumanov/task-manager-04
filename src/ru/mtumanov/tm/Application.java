package ru.mtumanov.tm;

import static ru.mtumanov.tm.constant.TerminalConstant.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CMD_ABOUT:
                showAbout();
            break;
            case CMD_HELP:
                showHelp();
            break;
            case CMD_VERSION:
                showVersion();
            break;
        }
    }

    private static void showError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Maksim Tumanov");
        System.out.println("e-mail: mytumanov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show about program.\n", CMD_ABOUT);
        System.out.printf("%s - Show program version.\n", CMD_VERSION);
        System.out.printf("%s - Show list arguments.\n", CMD_HELP);
    }

}
